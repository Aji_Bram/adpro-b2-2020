package com.testbot.lineku.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.testbot.lineku.model.activity.*;
import com.testbot.lineku.model.exercises.PushUp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



public class Pushuptest {


    Workout pushupworkoutcustom;

    @BeforeEach
    public void setUp() {

        pushupworkoutcustom = new PushUp(new Day1(), 1, 1);

    }

    @Test
    public void testMethodgetnamePushUp() {
        String name = pushupworkoutcustom.getname();
        assertEquals(name, "PushUp");
    }

    @Test
    public void testCustomTotalRepPushUp() {
        int totrep = pushupworkoutcustom.gettotalworkout();
        assertEquals(1,totrep);
    }




}
