package com.testbot.lineku.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.testbot.lineku.model.activity.*;
import com.testbot.lineku.model.exercises.PullUp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;





public class Pulluptest {

    Workout pullupworkoutcustom;

    @BeforeEach
    public void setUp() {

        pullupworkoutcustom = new PullUp(new Day1(), 1, 1);
    }

    @Test
    public void testMethodgetnamePullUp() {
        String name = pullupworkoutcustom.getname();
        assertEquals(name, "PullUp");
    }




    @Test
    public void testCustomTotalRepPullUp() {
        int totrep = pullupworkoutcustom.gettotalworkout();
        assertEquals(1,totrep);
    }



}
