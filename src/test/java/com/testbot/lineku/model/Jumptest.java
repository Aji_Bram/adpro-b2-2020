package com.testbot.lineku.model;


import static org.junit.jupiter.api.Assertions.assertEquals;

import com.testbot.lineku.model.activity.*;
import com.testbot.lineku.model.exercises.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;




public class Jumptest {

    Workout jumpworkoutcustom;

    @BeforeEach
    public void setUp() {

        jumpworkoutcustom = new Jump(new Day1(), 1, 1);
    }

    @Test
    public void testMethodgetnameJump() {
        String name = jumpworkoutcustom.getname();
        assertEquals(name, "Jump");
    }





    @Test
    public void testCustomTotalRepJump() {
        int totrep = jumpworkoutcustom.gettotalworkout();
        assertEquals(1,totrep);
    }




}

