package com.testbot.lineku.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.testbot.lineku.model.activity.*;
import com.testbot.lineku.model.exercises.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



public class Jadwaltest {

    Workout day1;
    Workout day2;
    Workout day3;
    Workout day4;
    Workout day5;
    Workout day6;
    Workout day7;


    /** Method SetUp Test.
     */
    @BeforeEach
    public void setUp() {
        day1 = new Day1();
        day2 = new Day2();
        day3 = new Day3();
        day4 = new Day4();
        day5 = new Day5();
        day6 = new Day6();
        day7 = new Day7();


    }


    /** Method Test.
     */
    @Test
    public void testMethodJadwalday1() {
        WorkoutDecorator jump = new Jump(day1, 3, 2);
        WorkoutDecorator pushup = new PushUp(jump, 3, 2);
        String dayname = day1.getname();

        final int totalWorkoutDay1 = pushup.gettotalworkout();
        assertEquals(dayname,"Day 1:");

        assertEquals(totalWorkoutDay1, 12);
    }

    /** Method Test.
     */
    @Test
    public void testMethodJadwalday2() {
        WorkoutDecorator pullup = new PullUp(day2, 3, 2);
        WorkoutDecorator situp = new SitUp(pullup, 3, 2);
        String dayname = day2.getname();

        final int totalWorkoutDay2 = situp.gettotalworkout();
        assertEquals(dayname,"Day 2:");

        assertEquals(totalWorkoutDay2, 12);
    }

    /** Method Test.
     */
    @Test
    public void testMethodJadwalday3() {
        WorkoutDecorator pullup = new PullUp(day3, 3, 2);
        WorkoutDecorator situp = new SitUp(pullup, 3, 2);
        WorkoutDecorator jump = new Jump(situp, 3, 2);
        String dayname = day3.getname();

        final int totalWorkoutDay3 = jump.gettotalworkout();
        assertEquals(dayname,"Day 3:");

        assertEquals(totalWorkoutDay3, 18);
    }

    /** Method Test.
     */
    @Test
    public void testMethodJadwalday4() {
        WorkoutDecorator pullup = new PullUp(day4, 3, 2);
        WorkoutDecorator situp = new SitUp(pullup, 3, 2);
        WorkoutDecorator jump = new Jump(situp, 3, 2);
        String dayname = day4.getname();

        final int totalWorkoutDay4 = jump.gettotalworkout();
        assertEquals(dayname,"Day 4:");

        assertEquals(totalWorkoutDay4, 18);
    }

    /** Method Test.
     */
    @Test
    public void testMethodJadwalday5() {
        WorkoutDecorator pullup = new PullUp(day5, 3, 2);
        WorkoutDecorator situp = new SitUp(pullup, 3, 2);
        WorkoutDecorator jump = new Jump(situp, 3, 2);
        String dayname = day5.getname();

        final int totalWorkoutDay5 = jump.gettotalworkout();
        assertEquals(dayname,"Day 5:");

        assertEquals(totalWorkoutDay5, 18);
    }

    /** Method Test.
     */
    @Test
    public void testMethodJadwalday6() {
        WorkoutDecorator pullup = new PullUp(day6, 3, 2);
        WorkoutDecorator situp = new SitUp(pullup, 3, 2);
        WorkoutDecorator jump = new Jump(situp, 3, 2);
        String dayname = day6.getname();

        final int totalWorkoutDay6 = jump.gettotalworkout();
        assertEquals(dayname,"Day 6:");

        assertEquals(totalWorkoutDay6, 18);
    }

    /** Method Test.
     */
    @Test
    public void testMethodJadwalday7() {
        WorkoutDecorator pullup = new PullUp(day7, 3, 2);
        WorkoutDecorator situp = new SitUp(pullup, 3, 2);
        WorkoutDecorator jump = new Jump(situp, 3,2);
        String dayname = day7.getname();
        final int totalWorkoutDay7 = jump.gettotalworkout();
        assertEquals(dayname,"Day 7:");

        assertEquals(totalWorkoutDay7, 18);
    }










}
