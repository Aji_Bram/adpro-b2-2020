package com.testbot.lineku.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



public class Workoutplantest {

    Workoutplanner workoutplanner;


    @BeforeEach
    void setUp() {
        workoutplanner = new Workoutplanner("userid", "Day 1", "Jump",
                3,5,15);
    }


    @Test
    void getuserid() {
        String id = workoutplanner.getuserid();
        assertEquals(id, "userid");
    }

    @Test
    void getuserDay() {
        String day = workoutplanner.getday();
        assertEquals(day, "Day 1");
    }

    @Test
    void getuserworkout() {
        String workoutname = workoutplanner.getworkoutname();
        assertEquals(workoutname, "Jump");
    }

    @Test
    void getuserset() {
        int set = workoutplanner.getworkoutset();
        assertEquals(set, 3);
    }

    @Test
    void getuserrep() {
        int rep = workoutplanner.getworkoutrep();
        assertEquals(rep, 5);
    }

    @Test
    void gettotalworkout() {
        int total = workoutplanner.gettotalworkout();
        assertEquals(total, 15);
    }

    @Test
    void setuserrep() {
        workoutplanner.setworkoutrep(10);
        int rep = workoutplanner.getworkoutrep();
        assertEquals(rep, 10);
    }

    @Test
    void setuserset() {
        workoutplanner.setworkoutset(2);
        int set = workoutplanner.getworkoutset();
        assertEquals(set, 2);
    }



}
