package com.testbot.lineku.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;



@SpringBootTest
public class Usertest {

    User user;

    @BeforeEach
    void setup() {
        user = new User("userid", "lineid", "ladorayhann");
    }

    @Test
    void getuserid() {
        String id = user.getuserid();
        assertEquals(id, "userid");
    }

    @Test
    void getlineid() {
        String lineId = user.getlineid();
        assertEquals(lineId, "lineid");
    }

    @Test
    void getdn() {
        String dn = user.getdisplayname();
        assertEquals(dn, "ladorayhann");
    }

}
