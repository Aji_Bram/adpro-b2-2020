package com.testbot.lineku.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.testbot.lineku.model.activity.Day1;
import com.testbot.lineku.model.exercises.SitUp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class Situptest {

    Workout situpworkoutcustom;

    @BeforeEach
    public void setUp() {

        situpworkoutcustom = new SitUp(new Day1(), 1, 1);

    }

    @Test
    public void testMethodgetnameSitUp() {
        String name = situpworkoutcustom.getname();
        assertEquals(name, "SitUp");
    }

    @Test
    public void testCustomTotalRepSitUp() {
        int totrep = situpworkoutcustom.gettotalworkout();
        assertEquals(1,totrep);
    }



}

