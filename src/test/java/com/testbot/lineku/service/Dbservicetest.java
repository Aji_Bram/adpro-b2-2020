package com.testbot.lineku.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;

import com.testbot.lineku.database.Dao;
import com.testbot.lineku.database.Workoutdb;
import com.testbot.lineku.model.User;
import java.util.ArrayList;
import java.util.List;

import com.testbot.lineku.model.Workoutplanner;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;



@ExtendWith(MockitoExtension.class)
public class Dbservicetest {


    @Mock
    private Dao mdao;

    @Mock
    private Workoutdb workoutDB;

    @InjectMocks
    private Dbservice dbService;

    @Test
    void registerUser() {
        when(mdao.registerLineId("userid", "ladorayhann", "lado"))
                .thenReturn(1);
        int check = mdao.registerLineId("userid", "ladorayhann", "lado");
        assertEquals(check, 1);
        verify(mdao, times(1)).registerLineId("userid", "ladorayhann", "lado");


    }



    @Test
    void findUserById() {
        List<User> users = new ArrayList<>();

        when(mdao.getByUserId("userid"))
                .thenReturn(users);
        String user = dbService.findUserById("userid");
        assertEquals(user, null);
        mdao.getByUserId("userid");
        verify(mdao, times(2)).getByUserId("userid");

        users.add(new User("userid", "lado", "lado"));
        when(mdao.getByUserId("userid"))
                .thenReturn(users);
        user = dbService.findUserById("userid");
        assertEquals("userid", user);
        verify(mdao, times(3)).getByUserId("userid");
    }



    @Test
    void save() {
        dbService.saveRecord("userid", "Day 1", "Jump",
                3, 5, 15);
        when(workoutDB.saverecord("userid", "Day 1", "Jump",
                3, 5, 15)).thenReturn(1);
        int a = workoutDB.saverecord("userid", "Day 1", "Jump",
                3, 5, 15);
        assertEquals(a,1);
        verify(workoutDB).saverecord("userid", "Day 1",
                "Jump",
                3,5,15);

        int b = dbService.saveRecord("userid", "Day 1", "Jump",
                3, 5, 15);
        assertEquals(b, -1);
    }


    @Test
    void findAll() {
        dbService.regLineID("userid", "ladorayhann", "lado");
        when(mdao.registerLineId("userid", "ladorayhann", "lado"))
                .thenReturn(1);
        mdao.registerLineId("userid", "ladorayhann", "lado");

        List<User> lst = mdao.get();

        assertEquals(lst.size(), dbService.findAll().size());
    }

    @Test
    void findUserByIdInvalid() {

        int hasil = dbService.regLineID("id", "ladorayhann", "lado");

        assertEquals(hasil, 0);

    }

    @Test
    void updateTotal() {
        dbService.updateRecord("userid",10, "senin");
        when(workoutDB.updatetotal("userid","senin", 10)).thenReturn(1);
        int a = workoutDB.updatetotal("userid", "senin", 10);
        assertEquals(a,1);
        verify(workoutDB).updatetotal("userid","senin", 10);

    }

    @Test
    void findWorkout() {
        List<Workoutplanner> lst = new ArrayList<>();
        when(workoutDB.getbyuserid("userId"))
                .thenReturn(lst);
        List<Workoutplanner> lst2 = dbService.findUserByIdWorkout("userId");
        assertEquals(lst.size(), lst2.size());

        lst.add(new Workoutplanner("userId", "senin", "push up",
                2, 3, 6));
        when(workoutDB.getbyuserid("userId"))
                .thenReturn(lst);
        lst2 = dbService.findUserByIdWorkout("userId");
        assertEquals(lst.size(), lst2.size());
    }


    @Test
    void deleteexercise() {
        dbService.deleteexercise("userId", "senin");
        when(workoutDB.delete("userId", "senin")).thenReturn(1);
        int a = workoutDB.delete("userId", "senin");
        assertEquals(a, 1);
        verify(workoutDB).delete("userId", "senin");

        int b = dbService.deleteexercise("userId", "senin");
        assertEquals(b, -1);
    }

    @Test
    void deleteexerciseall() {
        dbService.deleteexerciseall("userId");
        when(workoutDB.deleteAll("userId")).thenReturn(1);
        int a = workoutDB.deleteAll("userId");
        assertEquals(a, 1);
        verify(workoutDB).deleteAll("userId");

        int b = dbService.deleteexerciseall("userId");
        assertEquals(b, -1);
    }




}
