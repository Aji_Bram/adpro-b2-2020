package com.testbot.lineku.service;


import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.message.UnknownMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.linecorp.bot.model.response.BotApiResponse;
import com.testbot.lineku.database.Dao;
import com.testbot.lineku.model.Workout;
import com.testbot.lineku.model.Workoutplanner;
import com.testbot.lineku.model.activity.*;
import com.testbot.lineku.model.exercises.Jump;
import com.testbot.lineku.model.exercises.PullUp;
import com.testbot.lineku.model.exercises.PushUp;
import com.testbot.lineku.model.exercises.SitUp;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class BotServiceTest {

    @Mock
    private LineMessagingClient lineMessagingClient;

    @InjectMocks
    private Botservice botservice;

    @Mock
    Dao mdao;

    @Mock
    private Source source;

    @Mock
    Dbservice dbservice;




    @Test
    void handleMessageEventRandomMessage() {
        String replytoken = "replytoken";
        String message = "hai";
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat Datang Di Adprofit. "
                        + "Untuk mengetahui command apa yang bisa anda jalankan. Ketik '/help'."
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        botservice.handleOneOnOneChats(replytoken, message);
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat Datang Di Adprofit. "
                        + "Untuk mengetahui command apa yang bisa anda jalankan. Ketik '/help'."
        ))
        ));
    }

    @Test
    void handleMessageEventhelp() {
        String replytoken = "replytoken";
        String message = "/help";
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Halo Admin akan membantumu dengan command-command yang ada di Adprofit\n\n"
                        + "1. Jika tidak tahu atau lupa dengan command yang ada, "
                        + "maka anda dapat mengetik command berikut '/help'\n\n"
                        + "2. Jika anda ingin join maka anda dapat mengetik command berikut "
                        + "'join {@idline}' - Gunakan @ dan tanda { dan } tidak perlu dituliskan. "
                        + "Contoh 'join @ladorayhann'\n\n"
                        + "3. Jika anda sudah join dan sudah membuat jadwal. "
                        + "Anda dapat melihat jadwal anda dengan command 'jadwal'\n\n"
                        + "4. Jika sudah terdaftar anda dapat membuat jadwal workout. "
                        + "Ketik Command 'create'.\n\n"
                        + "5. Jika ingin delete exercise per hari ketik command 'delete {hari}'\n\n"
                        + "6. Jika ingin clear semua exercise ketik command 'clear'\n\n"
                        + "7. Jika sudah create dalam suatu hari tidak dapat create kembali "
                        + "di hari yang sama.\n\n"
                        + "8. Jika ingin mengganti exercise anda dapat delete exercise di hari tersebut, "
                        + "create kembali.\n\n"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        botservice.handleOneOnOneChats(replytoken, message);
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Halo Admin akan membantumu dengan command-command yang ada di Adprofit\n\n"
                        + "1. Jika tidak tahu atau lupa dengan command yang ada, "
                        + "maka anda dapat mengetik command berikut '/help'\n\n"
                        + "2. Jika anda ingin join maka anda dapat mengetik command berikut "
                        + "'join {@idline}' - Gunakan @ dan tanda { dan } tidak perlu dituliskan. "
                        + "Contoh 'join @ladorayhann'\n\n"
                        + "3. Jika anda sudah join dan sudah membuat jadwal. "
                        + "Anda dapat melihat jadwal anda dengan command 'jadwal'\n\n"
                        + "4. Jika sudah terdaftar anda dapat membuat jadwal workout. "
                        + "Ketik Command 'create'.\n\n"
                        + "5. Jika ingin delete exercise per hari ketik command 'delete {hari}'\n\n"
                        + "6. Jika ingin clear semua exercise ketik command 'clear'\n\n"
                        + "7. Jika sudah create dalam suatu hari tidak dapat create kembali "
                        + "di hari yang sama.\n\n"
                        + "8. Jika ingin mengganti exercise anda dapat delete exercise di hari tersebut, "
                        + "create kembali.\n\n"
        ))
        ));
    }


    @Test
    void handleMessageEventCreate() {
        String replytoken = "replytoken";
        String message = "create";
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Terdapat Workout Exercise.\n\n"
                        + "1. 'Push Up'\n"
                        + "2. 'Pull Up'\n"
                        + "3. 'Sit Up'\n"
                        + "4. 'Jump'\n\n"
                        + "Cara menyusun jadwal seperti format berikut :\n\n"
                        + "[{Hari}-{Nama_Exercise}-{Jumlah_Set}-"
                        + "{Jumlah_Rep},{Nama_Exercise}-{Jumlah_Set}-{Jumlah_Rep}]\n\n"
                        + "Contoh : [Senin-Push Up-3-5, Sit Up-2-4]"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        botservice.handleOneOnOneChats(replytoken, message);
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Terdapat Workout Exercise.\n\n"
                        + "1. 'Push Up'\n"
                        + "2. 'Pull Up'\n"
                        + "3. 'Sit Up'\n"
                        + "4. 'Jump'\n\n"
                        + "Cara menyusun jadwal seperti format berikut :\n\n"
                        + "[{Hari}-{Nama_Exercise}-{Jumlah_Set}-"
                        + "{Jumlah_Rep},{Nama_Exercise}-{Jumlah_Set}-{Jumlah_Rep}]\n\n"
                        + "Contoh : [Senin-Push Up-3-5, Sit Up-2-4]"
        ))
        ));
    }

    @Test
    void profiletest() {

        when(lineMessagingClient.getProfile("U475de0ce29786cf51fc090cc536e76e0")
        ).thenReturn(CompletableFuture.completedFuture(new UserProfileResponse("Lado Rayhan",
                "U475de0ce29786cf51fc090cc536e76e0 ",
                "https://profile.line-scdn.net/0hd9-lJplwOxYQFROTO7VEQSxQNXtnOz1eaHokdjERYnE-cnkVJXcgdTUSYyU6Jn9HKHNwdWdHMSJq",
                "off | wa")));
        botservice.getProfile("U475de0ce29786cf51fc090cc536e76e0");
        verify(lineMessagingClient).getProfile("U475de0ce29786cf51fc090cc536e76e0");
    }


    @Test
    void handleMessageEventCreateExerciseFailedDayName() {
        String replytoken = "replytoken";
        String message = "[jogja-Push Up-3-5,Sit Up-2-4]";
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Salah input nama hari. "
                        + "Berikut nama hari yang valid:\n\n"
                        + "senin, selasa, rabu, kamis, jumat, sabtu, minggu"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        botservice.handleOneOnOneChats(replytoken, message);
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Salah input nama hari. "
                        + "Berikut nama hari yang valid:\n\n"
                        + "senin, selasa, rabu, kamis, jumat, sabtu, minggu"
        ))
        ));
    }

    @Test
    void testcheckworkoutname() {
        Workout workout = botservice.chooseworkoutname("pushing", null, 3, 2);
        assertEquals(workout, null);
        workout = botservice.chooseworkoutname("push up", null, 3, 2);
        assertNotEquals(workout, new PushUp(null, 3, 2));
        workout = botservice.chooseworkoutname("pull up", null, 3, 2);
        assertNotEquals(workout, new PullUp(null, 3, 2));
        workout = botservice.chooseworkoutname("sit up", null, 3, 2);
        assertNotEquals(workout, new SitUp(null, 3, 2));
        workout = botservice.chooseworkoutname("jump", null, 3, 2);
        assertNotEquals(workout, new Jump(null, 3, 2));
    }

    @Test
    void testcheckday() {
        Workout workout = botservice.gettingday("kliwon");
        assertEquals(workout, null);
        workout = botservice.gettingday("senin");
        assertNotEquals(workout, new Day1());
        workout = botservice.gettingday("selasa");
        assertNotEquals(workout, new Day2());
        workout = botservice.gettingday("rabu");
        assertNotEquals(workout, new Day3());
        workout = botservice.gettingday("kamis");
        assertNotEquals(workout, new Day4());
        workout = botservice.gettingday("jumat");
        assertNotEquals(workout, new Day5());
        workout = botservice.gettingday("sabtu");
        assertNotEquals(workout, new Day6());
        workout = botservice.gettingday("minggu");
        assertNotEquals(workout, new Day7());

    }

    @Test
    void checkrepsetinput() {
        boolean hasil = botservice.checksetrepint("userid", "4", "6");
        assertEquals(hasil, true);

        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Set & Rep Harus Berupa Angka!"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        hasil = botservice.checksetrepint("replytoken", "sd", "123s");
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Set & Rep Harus Berupa Angka!"
        ))
        ));
        assertEquals(hasil, false);

    }

    @Test
    void checkdaymessage() {
        boolean hasil;
        hasil = botservice.checkday("senin");
        assertEquals(hasil, true);
        hasil = botservice.checkday("selasa");
        assertEquals(hasil, true);
        hasil = botservice.checkday("rabu");
        assertEquals(hasil, true);
        hasil = botservice.checkday("kamis");
        assertEquals(hasil, true);
        hasil = botservice.checkday("jumat");
        assertEquals(hasil, true);
        hasil = botservice.checkday("sabtu");
        assertEquals(hasil, true);
        hasil = botservice.checkday("minggu");
        assertEquals(hasil, true);
        hasil = botservice.checkday("Day1");
        assertEquals(hasil, false);

    }

    @Test
    void checkworkoutname() {
        boolean hasil;
        hasil = botservice.workoutnamechecker("push up");
        assertEquals(hasil, true);
        hasil = botservice.workoutnamechecker("pull up");
        assertEquals(hasil, true);
        hasil = botservice.workoutnamechecker("sit up");
        assertEquals(hasil, true);
        hasil = botservice.workoutnamechecker("jump");
        assertEquals(hasil, true);
        hasil = botservice.workoutnamechecker("squat");
        assertEquals(hasil, false);

    }


    @Test
    void greetingmesssage() {
        String replytoken = "replytoken";

        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat Datang Di Adprofit. "
                + "Untuk mengetahui command apa yang bisa anda jalankan. Ketik '/help'."
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        botservice.greetingMessage(replytoken);
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat Datang Di Adprofit. "
                + "Untuk mengetahui command apa yang bisa anda jalankan. Ketik '/help'."
        ))
        ));

    }

    @Test
    void insertingcheck() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat sudah menambahkan exercise!"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));
        when(dbservice.saveRecord("userId", "senin", "push up",
                3, 2, 6
        )).thenReturn(2);
        when(dbservice.updateRecord("userId", 6, "senin"
        )).thenReturn(2);
        when(dbservice.saveRecord("userId", "senin", "pull up",
                3, 2, 12
        )).thenReturn(2);
        when(dbservice.updateRecord("userId", 12, "senin"
        )).thenReturn(2);
        String [] lst = new String[]{"push up-3-2","pull up-3-2"};
        botservice.insertworkout("replytoken", "senin", lst);
        verify(lineMessagingClient, times(2)).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat sudah menambahkan exercise!"
        ))
        ));

    }

    @Test
    void insertingcheckfailed() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Salah input nama workout atau set dan rep. "
                        + "Berikut nama workout yang valid.\n\n"
                        + "push up, pull up, sit up, jump"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));
        String [] lst = new String[]{"pushing-3-2"};
        botservice.insertworkout("replytoken", "senin", lst);
        verify(lineMessagingClient, times(1)).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Salah input nama workout atau set dan rep. "
                        + "Berikut nama workout yang valid.\n\n"
                        + "push up, pull up, sit up, jump"
        ))
        ));

    }

    @Test
    void testjoinsucceed() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat sudah bergabung!"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));

        when(dbservice.regLineID("userId", "ladorayhann", "displayName"))
                .thenReturn(2);
        botservice.handleOneOnOneChats("replytoken",
                "join @ladorayhann");
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat sudah bergabung!"
        ))
        ));


    }


    @Test
    void testidlinelengthfailed() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "butuh lebih dari 3 karakter untuk menemukan user"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        botservice.handleOneOnOneChats("replytoken",
                "join @l");
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "butuh lebih dari 3 karakter untuk menemukan user"
        ))
        ));
    }

    @Test
    void testidlinelengthsendernull() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Hi, tambahkan dulu Adprofit sebagai teman"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        null
                ));

        botservice.handleOneOnOneChats("replytoken",
                "join @ladorayhann");
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Hi, tambahkan dulu Adprofit sebagai teman"
        ))
        ));


    }

    @Test
    void testidlinedisplaynamefailed() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "User tidak terdeteksi. Tambahkan Adprofit sebagai teman"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));

        botservice.checkDisplayNameAndLineId("replytoken",
                "");
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "User tidak terdeteksi. Tambahkan Adprofit sebagai teman"
        ))
        ));


    }

    @Test
    void handleMessageEvent() {
        final MessageEvent request = new MessageEvent<>(
                "replytoken",
                new UserSource("userId"),
                new TextMessageContent("id", "os"),
                Instant.now()
        );
        when(lineMessagingClient.getProfile("userId"))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));

        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat Datang Di Adprofit. "
                + "Untuk mengetahui command apa yang bisa anda jalankan. Ketik '/help'."
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        botservice.handleMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat Datang Di Adprofit. "
                        + "Untuk mengetahui command apa yang bisa anda jalankan. Ketik '/help'."
        ))
        ));
    }

    @Test
    void handleMessageEventGreeting() {
        final MessageEvent request = new MessageEvent<>(
                "replytoken",
                new UserSource("userId"),
                new UnknownMessageContent("id"),
                Instant.now()
        );
        when(lineMessagingClient.getProfile("userId"))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));

        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat Datang Di Adprofit. "
                        + "Untuk mengetahui command apa yang bisa anda jalankan. Ketik '/help'."
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        botservice.handleMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Selamat Datang Di Adprofit. "
                        + "Untuk mengetahui command apa yang bisa anda jalankan. Ketik '/help'."
        ))
        ));
    }


    @Test
    void checkJadwal() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Hari : senin\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : senin\nExercise : pull up\nSet : 3\nRep : 2\nTotal Workout : 12\n\n"
                + "Hari : selasa\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : rabu\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : kamis\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : jumat\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : sabtu\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : minggu\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"

        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));

        Workoutplanner workoutplanner = new Workoutplanner("userId",
                "senin", "push up",
                3, 2, 6);
        Workoutplanner workoutplanner2 = new Workoutplanner("userId",
                "selasa", "push up",
                3, 2, 6);
        Workoutplanner workoutplanner3 = new Workoutplanner("userId",
                "rabu", "push up",
                3, 2, 6);
        Workoutplanner workoutplanner4 = new Workoutplanner("userId",
                "kamis", "push up",
                3, 2, 6);
        Workoutplanner workoutplanner5 = new Workoutplanner("userId",
                "jumat", "push up",
                3, 2, 6);
        Workoutplanner workoutplanner6 = new Workoutplanner("userId",
                "sabtu", "push up",
                3, 2, 6);
        Workoutplanner workoutplanner7 = new Workoutplanner("userId",
                "minggu", "push up",
                3, 2, 6);
        Workoutplanner anothersenin = new Workoutplanner("userId",
                "senin", "pull up",
                3, 2, 12);
        List<Workoutplanner> lst = new ArrayList<>();
        lst.add(workoutplanner);
        lst.add(workoutplanner2);
        lst.add(workoutplanner3);
        lst.add(workoutplanner4);
        lst.add(workoutplanner5);
        lst.add(workoutplanner6);
        lst.add(workoutplanner7);
        lst.add(anothersenin);
        when(dbservice.findUserByIdWorkout("userId"
        )).thenReturn(lst);
        botservice.handleOneOnOneChats("replytoken", "jadwal");
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Hari : senin\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : senin\nExercise : pull up\nSet : 3\nRep : 2\nTotal Workout : 12\n\n"
                + "Hari : selasa\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : rabu\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : kamis\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : jumat\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : sabtu\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"
                + "Hari : minggu\nExercise : push up\nSet : 3\nRep : 2\nTotal Workout : 6\n\n"


        ))
        ));

    }

    @Test
    void testDuplicate() {
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));
        List<Workoutplanner> lst = new ArrayList<>();
        when(dbservice.findUserByIdWorkout("userId")).thenReturn(lst);
        boolean result = botservice.createChecker("senin");
        assertEquals(result, true);
        Workoutplanner workoutplanner = new Workoutplanner("userId",
                "senin", "push up",
                3, 2, 6);
        lst.add(workoutplanner);
        when(dbservice.findUserByIdWorkout("userId")).thenReturn(lst);
        result = botservice.createChecker("senin");
        assertEquals(result, false);


    }

    @Test
    void testdeleteexercise() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Berhasil Delete"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));
        when(dbservice.deleteexercise("userId", "senin")).thenReturn(1);
        botservice.handleOneOnOneChats("replytoken", "delete senin");
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Berhasil Delete"
        ))));
    }

    @Test
    void testdeleteexercisefailed() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Input Hari Salah"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));
        botservice.handleOneOnOneChats("replytoken", "delete seninun");
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Input Hari Salah"
        ))));
    }

    @Test
    void testdeleteexerciseall() {
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Berhasil Delete"
        ))
        ))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(lineMessagingClient.getProfile(null))
                .thenReturn(CompletableFuture.completedFuture(
                        new UserProfileResponse("displayName", "userId","","")
                ));
        when(dbservice.deleteexerciseall("userId")).thenReturn(1);
        botservice.handleOneOnOneChats("replytoken", "clear");
        verify(lineMessagingClient).replyMessage(new ReplyMessage(
                "replytoken", singletonList(new TextMessage(
                "Berhasil Delete"
        ))));
    }
}


