package com.testbot.lineku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinekuApplication {
    public static void main(String[] args) {
        SpringApplication.run(LinekuApplication.class, args);
    }
}

