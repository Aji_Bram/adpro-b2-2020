package com.testbot.lineku.model;

public class User {
    
    public String userid;

    public String lineid;

    public String displayname;

    /** Constructor User.
     */
    public User(String ui, String li, String dn) {
        userid = ui;
        lineid = li;
        displayname = dn;
    }

    /** Getter User.
     */
    public String getuserid() {
        return userid;
    }


    /** Getter User.
     */
    public String getdisplayname() {
        return displayname;
    }


    /** Getter User.
     */
    public String getlineid() {
        return lineid;
    }
}
