package com.testbot.lineku.model;

public class Workoutplanner {

    public String userid;
    public String day;
    public String workoutname;
    public int workoutset;
    public int workoutrep;
    public int totalworkout;

    /** Constructor Workout Planner.
     */
    public Workoutplanner(String ui,String d,String wn,int ws,int wr,int tw) {
        userid = ui;
        day = d;
        workoutname = wn;
        workoutset = ws;
        workoutrep = wr;
        totalworkout = tw;

    }

    /** Getter Workout Planner.
     */
    public int getworkoutrep() {
        return workoutrep;
    }

    /** Getter Workout Planner.
     */
    public int getworkoutset() {
        return workoutset;
    }

    /** Getter Workout Planner.
     */
    public String getday() {
        return day;
    }

    /** Getter Workout Planner.
     */
    public String getuserid() {
        return userid;
    }

    /** Getter Workout Planner.
     */
    public String getworkoutname() {
        return workoutname;
    }

    /** Getter Workout Planner.
     */
    public int gettotalworkout() {
        return totalworkout;
    }

    /** Getter Workout Planner.
     */
    public void setworkoutrep(int workoutrep) {
        this.workoutrep = workoutrep;
    }

    /** Getter Workout Planner.
     */
    public void setworkoutset(int workoutset) {
        this.workoutset = workoutset;
    }
}
