package com.testbot.lineku.model.exercises;

import com.testbot.lineku.model.Workout;

public abstract class WorkoutDecorator extends Workout {
    public abstract String getname();

}
