package com.testbot.lineku.model.exercises;

import com.testbot.lineku.model.Workout;

public class Jump extends WorkoutDecorator {
    Workout workout;
    int rep;
    int set;



    /** Constructor Jump.
     */
    public Jump(Workout workout, int reps, int sets) {
        this.workout = workout;
        this.rep = reps;
        this.set = sets;
    }


    /** Getter Name.
     */
    @Override
    public String getname() {
        return "Jump";
    }



    /** Getter Jump.
     */
    @Override
    public int gettotalworkout() {
        return workout.gettotalworkout() + (this.rep * this.set);
    }


}
