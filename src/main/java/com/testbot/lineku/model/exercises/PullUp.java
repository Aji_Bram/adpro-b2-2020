package com.testbot.lineku.model.exercises;

import com.testbot.lineku.model.Workout;

public class PullUp extends WorkoutDecorator {
    Workout workout;
    int rep;
    int set;



    /** Constructor Pull Up.
     */
    public PullUp(Workout workout,int reps, int sets) {
        this.workout = workout;
        this.rep = reps;
        this.set = sets;
    }

    /** Getter Pull Up.
     */
    @Override
    public String getname() {
        return "PullUp";
    }


    /** Getter Pull Up.
     */
    @Override
    public int gettotalworkout() {
        return workout.gettotalworkout() + (this.rep * this.set);
    }


}
