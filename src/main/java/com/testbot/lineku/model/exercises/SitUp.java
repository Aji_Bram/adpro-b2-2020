package com.testbot.lineku.model.exercises;

import com.testbot.lineku.model.Workout;

public class SitUp extends WorkoutDecorator {
    Workout workout;
    int rep;
    int set;




    /** Constructor Sit Up.
     */
    public SitUp(Workout workout, int reps, int sets) {
        this.workout = workout;
        this.rep = reps;
        this.set = sets;
    }


    /** Getter Sit Up.
     */
    @Override
    public String getname() {
        return "SitUp";
    }


    /** Getter Sit Up.
     */
    @Override
    public int gettotalworkout() {
        return workout.gettotalworkout() + (this.rep * this.set);
    }
}
