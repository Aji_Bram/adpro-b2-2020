package com.testbot.lineku.model.exercises;

import com.testbot.lineku.model.Workout;

public class PushUp extends WorkoutDecorator {
    Workout workout;
    int rep;
    int set;



    /** Constructor Push Up.
     */
    public PushUp(Workout workout, int reps, int sets) {
        this.workout = workout;
        this.rep = reps;
        this.set = sets;
    }

    /** Getter Push Up.
     */
    @Override
    public String getname() {
        return "PushUp";
    }


    /** Getter Push Up.
     */
    @Override
    public int gettotalworkout() {
        return workout.gettotalworkout() + (this.rep * this.set);
    }

}
