package com.testbot.lineku.service;

import com.testbot.lineku.database.Dao;
import com.testbot.lineku.database.Workoutdb;
import com.testbot.lineku.model.User;
import com.testbot.lineku.model.Workoutplanner;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class Dbservice {

    @Autowired
    private Dao mdao;

    @Autowired
    private Workoutdb workoutDB;


    /** method mendaftarkan LINE ID.
     */
    public int regLineID(String userid, String lineid, String displayname) {
        if (findUserById(userid) == null) {
            return mdao.registerLineId(userid, lineid, displayname);
        }

        return -1;
    }



    /** method untuk mencari user terdaftar di database.
     */
    public String findUserById(String userId) {
        List<User> users = mdao.getByUserId(userId);

        if (users.size() > 0) {
            return users.get(0).getuserid();
        }
        return null;
    }

    /** method untuk mencari user terdaftar di database.
     */
    public List<Workoutplanner> findUserByIdWorkout(String userId) {
        List<Workoutplanner> usersdata = workoutDB.getbyuserid(userId);
        return usersdata;
    }

    /** method untuk mencari semua user terdaftar di database.
     */
    public List<User> findAll() {
        return mdao.get();
    }

    /** method untuk save data user di database.
     */
    public int saveRecord(String userId, String userDay,
                          String workoutName, int workoutSet,
                          int workoutRep, int totalworkout) {

        if (findUserById(userId) != null) {
            return workoutDB.saverecord(userId, userDay,
                    workoutName, workoutSet,
                    workoutRep, totalworkout);
        }

        return -1;

    }

    /** method untuk update data user ke database.
     * */
    public int updateRecord(String userid, int total, String day) {
        if (findUserById(userid) != null) {
            return workoutDB.updatetotal(userid, day, total);
        }

        return -1;
    }

    /** method untuk delete exercise per hari.
     * */
    public int deleteexercise(String userid, String day) {
        if (findUserById(userid) != null) {
            return workoutDB.delete(userid, day);
        }

        return -1;
    }

    /** method untuk delete exercise all.
     * */
    public int deleteexerciseall(String userid) {
        if (findUserById(userid) != null) {
            return workoutDB.deleteAll(userid);
        }

        return -1;
    }

}