package com.testbot.lineku.service;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.MessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.testbot.lineku.database.Workoutdb;
import com.testbot.lineku.model.Workout;
import com.testbot.lineku.model.Workoutplanner;
import com.testbot.lineku.model.activity.*;
import com.testbot.lineku.model.exercises.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




@Service
public class Botservice {

    public Source source;

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private Dbservice dbService;

    @Autowired
    Workoutdb workoutdb;


    private void reply(ReplyMessage replyMessage) {
        try {
            lineMessagingClient.replyMessage(replyMessage).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public void reply(String replyToken, Message message) {
        ReplyMessage replyMessage = new ReplyMessage(replyToken, message);
        reply(replyMessage);
    }

    /** Get User From Line.
     */
    public UserProfileResponse getProfile(String userId) {
        try {
            return lineMessagingClient.getProfile(userId).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    /** Method To ReplyText.
     */
    public void replyText(String replyToken, String messageText) {
        TextMessage textMessage = new TextMessage(messageText);
        reply(replyToken, textMessage);
    }

    /** Method To Create Greeting Message.
     */
    public void greetingMessage(String replyToken) {

        String help = "Untuk mengetahui command apa yang bisa anda jalankan. Ketik '/help'.";
        String welcoming = "Selamat Datang Di Adprofit. ";
        replyText(replyToken, welcoming + help);

    }



    /** Method To Handle Message Event.
     */
    public void handleMessageEvent(MessageEvent event) {
        String replyToken      = event.getReplyToken();
        MessageContent content = event.getMessage();
        Source source          = event.getSource();
        String senderId        = source.getSenderId();
        UserProfileResponse sender = getProfile(senderId);

        if (content instanceof TextMessageContent) {
            handleTextMessage(replyToken, (TextMessageContent) content, source);
        } else {
            greetingMessage(replyToken);
        }
    }

    /** Method To Handle Text Message.
     */
    public void handleTextMessage(String replyToken, TextMessageContent content, Source source) {
        if (source instanceof UserSource) {
            handleOneOnOneChats(replyToken, content.getText());
        }
    }

    /** Method To Handle Reply Jadwal.
     */
    public void replyJadwal(String replyToken) {

        String senin = jadwalSenin();
        String selasa = jadwalSelasa();
        String rabu = jadwalRabu();
        String kamis = jadwalKamis();
        String jumat = jadwalJumat();
        String sabtu = jadwalSabtu();
        String minggu = jadwalMinggu();
        String hasil = senin + selasa + rabu + kamis + jumat + sabtu + minggu;
        replyText(replyToken, hasil);

    }

    private String jadwalMinggu() {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        List<Workoutplanner> lst = dbService.findUserByIdWorkout(sender.getUserId());
        String jadwalminggu = "";

        for (Workoutplanner workoutplanner : lst) {
            if (workoutplanner.day.equals("minggu")) {
                jadwalminggu += "Hari : " + workoutplanner.getday() + "\n"
                        + "Exercise : " + workoutplanner.getworkoutname() + "\n"
                        + "Set : " + workoutplanner.getworkoutset() + "\n"
                        + "Rep : " + workoutplanner.getworkoutrep() + "\n"
                        + "Total Workout : " + workoutplanner.gettotalworkout()
                        + "\n\n";
            }
        }

        return  jadwalminggu;
    }

    private String jadwalSabtu() {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        List<Workoutplanner> lst = dbService.findUserByIdWorkout(sender.getUserId());
        String jadwalsabtu = "";

        for (Workoutplanner workoutplanner : lst) {
            if (workoutplanner.day.equals("sabtu")) {
                jadwalsabtu += "Hari : " + workoutplanner.getday() + "\n"
                        + "Exercise : " + workoutplanner.getworkoutname() + "\n"
                        + "Set : " + workoutplanner.getworkoutset() + "\n"
                        + "Rep : " + workoutplanner.getworkoutrep() + "\n"
                        + "Total Workout : " + workoutplanner.gettotalworkout()
                        + "\n\n";
            }
        }


        return  jadwalsabtu;
    }



    private String jadwalJumat() {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        List<Workoutplanner> lst = dbService.findUserByIdWorkout(sender.getUserId());
        String jadwaljumat = "";

        for (Workoutplanner workoutplanner : lst) {
            if (workoutplanner.day.equals("jumat")) {
                jadwaljumat += "Hari : " + workoutplanner.getday() + "\n"
                        + "Exercise : " + workoutplanner.getworkoutname() + "\n"
                        + "Set : " + workoutplanner.getworkoutset() + "\n"
                        + "Rep : " + workoutplanner.getworkoutrep() + "\n"
                        + "Total Workout : " + workoutplanner.gettotalworkout()
                        + "\n\n";
            }
        }

        return  jadwaljumat;
    }

    private String jadwalKamis() {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        List<Workoutplanner> lst = dbService.findUserByIdWorkout(sender.getUserId());
        String jadwalkamis = "";

        for (Workoutplanner workoutplanner : lst) {
            if (workoutplanner.day.equals("kamis")) {
                jadwalkamis += "Hari : " + workoutplanner.getday() + "\n"
                        + "Exercise : " + workoutplanner.getworkoutname() + "\n"
                        + "Set : " + workoutplanner.getworkoutset() + "\n"
                        + "Rep : " + workoutplanner.getworkoutrep() + "\n"
                        + "Total Workout : " + workoutplanner.gettotalworkout()
                        + "\n\n";
            }
        }

        return  jadwalkamis;
    }

    private String jadwalRabu() {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        List<Workoutplanner> lst = dbService.findUserByIdWorkout(sender.getUserId());
        String jadwalrabu = "";

        for (Workoutplanner workoutplanner : lst) {
            if (workoutplanner.day.equals("rabu")) {
                jadwalrabu += "Hari : " + workoutplanner.getday() + "\n"
                        + "Exercise : " + workoutplanner.getworkoutname() + "\n"
                        + "Set : " + workoutplanner.getworkoutset() + "\n"
                        + "Rep : " + workoutplanner.getworkoutrep() + "\n"
                        + "Total Workout : " + workoutplanner.gettotalworkout()
                        + "\n\n";
            }
        }

        return  jadwalrabu;
    }

    private String jadwalSelasa() {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        List<Workoutplanner> lst = dbService.findUserByIdWorkout(sender.getUserId());
        String jadwalselasa = "";

        for (Workoutplanner workoutplanner : lst) {
            if (workoutplanner.day.equals("selasa")) {
                jadwalselasa += "Hari : " + workoutplanner.getday() + "\n"
                        + "Exercise : " + workoutplanner.getworkoutname() + "\n"
                        + "Set : " + workoutplanner.getworkoutset() + "\n"
                        + "Rep : " + workoutplanner.getworkoutrep() + "\n"
                        + "Total Workout : " + workoutplanner.gettotalworkout()
                        + "\n\n";
            }
        }

        return  jadwalselasa;
    }

    private String jadwalSenin() {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        List<Workoutplanner> lst = dbService.findUserByIdWorkout(sender.getUserId());
        String jadwalsenin = "";

        for (Workoutplanner workoutplanner : lst) {
            if (workoutplanner.day.equals("senin")) {
                jadwalsenin += "Hari : " + workoutplanner.getday() + "\n"
                        + "Exercise : " + workoutplanner.getworkoutname() + "\n"
                        + "Set : " + workoutplanner.getworkoutset() + "\n"
                        + "Rep : " + workoutplanner.getworkoutrep() + "\n"
                        + "Total Workout : " + workoutplanner.gettotalworkout()
                        + "\n\n";
            }
        }
        return  jadwalsenin;

    }


    /** Method To Handle Reply Help Message.
     */
    public void replyHelp(String replyToken) {
        String help = "Halo Admin akan membantumu dengan command-command yang ada di Adprofit\n\n"
                + "1. Jika tidak tahu atau lupa dengan command yang ada, "
                + "maka anda dapat mengetik command berikut '/help'\n\n"
                + "2. Jika anda ingin join maka anda dapat mengetik command berikut "
                + "'join {@idline}' - Gunakan @ dan tanda { dan } tidak perlu dituliskan. "
                + "Contoh 'join @ladorayhann'\n\n"
                + "3. Jika anda sudah join dan sudah membuat jadwal. "
                + "Anda dapat melihat jadwal anda dengan command 'jadwal'\n\n"
                + "4. Jika sudah terdaftar anda dapat membuat jadwal workout. "
                + "Ketik Command 'create'.\n\n"
                + "5. Jika ingin delete exercise per hari ketik command 'delete {hari}'\n\n"
                + "6. Jika ingin clear semua exercise ketik command 'clear'\n\n"
                + "7. Jika sudah create dalam suatu hari tidak dapat create kembali "
                + "di hari yang sama.\n\n"
                + "8. Jika ingin mengganti exercise anda dapat delete exercise di hari tersebut, "
                + "create kembali.\n\n";


        replyText(replyToken, help);
    }

    /** Method To Handle One On One Chat.
     */
    public void handleOneOnOneChats(String replyToken, String textMessage) {
        String msgText = textMessage.toLowerCase();
        if (msgText.contains("/help")) {
            replyHelp(replyToken);
        } else if (msgText.contains("jadwal")) {
            replyJadwal(replyToken);
        } else if (msgText.contains("create")) {
            replyCreate(replyToken);
        } else if  (msgText.contains("delete")) {
            replyDelete(replyToken, textMessage);
        } else if (msgText.contains("clear")) {
            replyDeleteAll(replyToken);
        } else if (msgText.contains("join")) {
            replyJoin(replyToken, msgText);
        } else if (msgText.substring(0,1).equals("[")
                && msgText.substring(msgText.length() - 1).equals("]")) {
            handleWorkout(replyToken, msgText);
        } else {
            String welcome = "Selamat Datang Di Adprofit. ";
            String help = "Untuk mengetahui command apa yang bisa anda jalankan."
                    + " Ketik '/help'.";
            replyText(replyToken, welcome + help);
        }
    }

    private void replyDeleteAll(String replyToken) {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);

        if (dbService.deleteexerciseall(sender.getUserId()) != 0) {
            replyText(replyToken, "Berhasil Delete");
        }
    }

    private void replyDelete(String replyToken, String text) {
        String day = text.split(" ")[1];
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        if (checkday(day)) {
            if (dbService.deleteexercise(sender.getUserId(), day) != 0) {
                replyText(replyToken, "Berhasil Delete");
            }
        } else {
            replyText(replyToken, "Input Hari Salah");
        }

    }

    /** Manipulasi input data workout.
     */
    public void handleWorkout(String replyToken, String textMessage) {
        int index = 0;
        for (int i = 0;i < textMessage.length();i++) {
            if (textMessage.substring(i,i + 1).equals("-")) {
                index = i;
                break;
            }
        }

        String day = textMessage.substring(1,index);
        String exercise = textMessage.substring(index + 1, textMessage.length() - 1);
        String [] exerciselst = exercise.split(", ");

        if (checkday(day)) {
            insertworkout(replyToken, day, exerciselst);
        } else {
            replyText(replyToken, "Salah input nama hari. "
                    + "Berikut nama hari yang valid:\n\n"
                    + "senin, selasa, rabu, kamis, jumat, sabtu, minggu");
        }
    }

    /** Day Checker.
     */
    public boolean checkday(String day) {
        if (day.toLowerCase().equals("senin")) {
            return true;
        } else if (day.toLowerCase().equals("selasa")) {
            return true;
        } else if (day.toLowerCase().equals("rabu")) {
            return true;
        } else if (day.toLowerCase().equals("kamis")) {
            return true;
        } else if (day.toLowerCase().equals("jumat")) {
            return true;
        } else if (day.toLowerCase().equals("sabtu")) {
            return true;
        } else if (day.toLowerCase().equals("minggu")) {
            return true;
        } else {
            return false;
        }

    }

    /** Inserting Workout to Database.
     * */
    public void insertworkout(String replyToken, String day, String[] exerciselst) {

        String[] exercisesplitter;

        String workoutname;
        int set;
        int rep;

        Workout workout = gettingday(day);
        WorkoutDecorator workoutdecorator = null;

        if (createChecker(day)) {
            for (String exercise : exerciselst) {
                exercisesplitter = exercise.split("-");
                if (workoutnamechecker(exercisesplitter[0])
                        && checksetrepint(replyToken, exercisesplitter[1],
                        exercisesplitter[2])) {
                    workoutname = exercisesplitter[0];
                    set = Integer.parseInt(exercisesplitter[1]);
                    rep = Integer.parseInt(exercisesplitter[2]);
                    if (exercise.equals(exerciselst[0])) {
                        workoutdecorator = chooseworkoutname(workoutname, workout, rep, set);
                    } else {
                        workoutdecorator = chooseworkoutname(workoutname,
                                workoutdecorator, rep, set);
                    }
                    inserting(replyToken, day, workoutname, set, rep,
                            workoutdecorator.gettotalworkout(), workoutdecorator);
                } else {
                    replyText(replyToken, "Salah input nama workout atau set dan rep. "
                            + "Berikut nama workout yang valid.\n\n"
                            + "push up, pull up, sit up, jump");
                }
            }
        } else {
            replyText(replyToken, "Tidak dapat create pada hari yang sudah dibuat.\n"
                    + "Lakukan delete atau clear terlebih dahulu pada hari tersebut");
        }



    }

    /** Method untuk cek ketika sudah create exercise
     *      pada suatu hari tidak dapat create di hari yang sama.
     *
     */
    public boolean createChecker(String day) {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        List<Workoutplanner> lst = dbService.findUserByIdWorkout(sender.getUserId());
        List<Workoutplanner> exerciseday = new ArrayList<>();
        for (Workoutplanner workoutplanner : lst) {
            if (workoutplanner.day.equals(day.toLowerCase())) {
                exerciseday.add(workoutplanner);
            }
        }
        return exerciseday.size() == 0;
    }

    /** Inserting Input To Database.
     * */
    public void inserting(String replyToken, String day,
                          String workout, int set, int rep,
                          int total, WorkoutDecorator workoutdecorator) {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        if (dbService.saveRecord(sender.getUserId(), day,
                workout, set, rep, total) != 0) {
            if (dbService.updateRecord(sender.getUserId(),
                    workoutdecorator.gettotalworkout(), day) != 0) {
                replyText(replyToken, "Selamat sudah menambahkan exercise!");
            } else {
                replyText(replyToken, "Gagal update");
            }

        } else {
            replyText(replyToken, "Gagal insert");
        }

    }



    /** Workout Name Checker.
     * */
    public WorkoutDecorator chooseworkoutname(String name,
                      Workout workout, int rep, int set) {
        if (name.toLowerCase().equals("push up")) {
            return new PushUp(workout, rep, set);
        } else if (name.toLowerCase().equals("pull up")) {
            return new PullUp(workout, rep, set);
        } else if (name.toLowerCase().equals("sit up")) {
            return new SitUp(workout, rep, set);
        } else if (name.toLowerCase().equals("jump")) {
            return new Jump(workout, rep, set);
        } else {
            return (WorkoutDecorator) workout;
        }
    }

    /** Day Checker And Assign Objek Day.
     * */
    public Workout gettingday(String day) {
        if (day.toLowerCase().equals("senin")) {
            return new Day1();
        } else if (day.toLowerCase().equals("selasa")) {
            return new Day2();
        } else if (day.toLowerCase().equals("rabu")) {
            return new Day3();
        } else if (day.toLowerCase().equals("kamis")) {
            return new Day4();
        } else if (day.toLowerCase().equals("jumat")) {
            return new Day5();
        } else if (day.toLowerCase().equals("sabtu")) {
            return new Day6();
        } else if (day.toLowerCase().equals("minggu")) {
            return new Day7();
        } else {
            return null;
        }
    }

    /** Check Set & Rep is an Integer.
     * */
    public boolean checksetrepint(String replyToken, String set, String rep) {

        try {
            Integer.parseInt(set);
            Integer.parseInt(rep);
        } catch (NumberFormatException e) {
            replyText(replyToken, "Set & Rep Harus Berupa Angka!");
            return false;
        }
        return true;
    }

    /** Workout Name Checker.
     * */
    public boolean workoutnamechecker(String name) {
        if (name.toLowerCase().equals("push up")) {
            return true;
        } else if (name.toLowerCase().equals("pull up")) {
            return true;
        } else if (name.toLowerCase().equals("sit up")) {
            return true;
        } else if (name.toLowerCase().equals("jump")) {
            return true;
        } else {
            return false;
        }
    }




    private void replyCreate(String replyToken) {
        String message = "Terdapat Workout Exercise.\n\n"
                + "1. 'Push Up'\n"
                + "2. 'Pull Up'\n"
                + "3. 'Sit Up'\n"
                + "4. 'Jump'\n\n"
                + "Cara menyusun jadwal seperti format berikut :\n\n"
                + "[{Hari}-{Nama_Exercise}-{Jumlah_Set}-"
                + "{Jumlah_Rep},{Nama_Exercise}-{Jumlah_Set}-{Jumlah_Rep}]\n\n"
                + "Contoh : [Senin-Push Up-3-5, Sit Up-2-4]";
        replyText(replyToken, message);
        
    }


    /** Method To Handle Reply Join Message.
     */
    public void replyJoin(String replyToken, String msg) {
        String [] words = msg.trim().split("\\s+");
        String target = words.length > 1 ? words[1] : "";

        checkUserLength(replyToken, target);

    }

    /** Method To Check User Length When Joining The App.
     */
    public void checkUserLength(String replyToken, String msg) {
        String registerMessage = null;
        if (msg.length() <= 3) {
            invalidUserLength(replyToken, registerMessage);
        } else {
            validUserLength(replyToken, msg);
        }

    }

    /** Method To Check User Length Invalid When Joining The App.
     */
    public void invalidUserLength(String replyToken, String msg) {
        msg = "butuh lebih dari 3 karakter untuk menemukan user";
        replyText(replyToken, msg);
    }

    /** Method To Check User Valid Length When Joining The App.
     */
    public void validUserLength(String replyToken, String msg) {
        String lineId = msg.substring(msg.indexOf("@") + 1);
        checkSender(replyToken, lineId);

    }

    /** Method To Check The Sender When Joining The App.
     */
    public void checkSender(String replyToken, String lineId) {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        if (sender != null) {
            checkDisplayNameAndLineId(replyToken, lineId);
        } else {
            replyText(replyToken, "Hi, tambahkan dulu Adprofit sebagai teman");
        }
    }

    /** Method To Check Sender Display Name And Line Id When Joining The App.
     */
    public void checkDisplayNameAndLineId(String replyToken, String lineId) {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        if (!sender.getDisplayName().isEmpty() && (lineId.length() > 0)) {
            checkRegistration(replyToken, lineId);
        } else {
            replyText(replyToken, "User tidak terdeteksi. Tambahkan Adprofit sebagai teman");
        }
    }


    /** Method To Check Registration When Joining The App.
     */
    public void checkRegistration(String replyToken, String lineId) {
        String userid = source.getSenderId();
        UserProfileResponse sender = getProfile(userid);
        if (dbService.regLineID(sender.getUserId(),
                lineId, sender.getDisplayName()) != 0) {
            replyText(replyToken, "Selamat sudah bergabung!");
        } else {
            replyText(replyToken, "Gagal Bergabung");
        }

    }



}