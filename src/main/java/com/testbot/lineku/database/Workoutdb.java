package com.testbot.lineku.database;

import com.testbot.lineku.model.Workoutplanner;
import java.util.List;

public interface Workoutdb {

    List<Workoutplanner> getbyuserid(String userid);

    int saverecord(String userid, String userday,
                   String workoutname, int workoutset, int workoutrep,
                   int totalworkout);

    int updatetotal(String userid, String day, int total);

    int delete(String userid, String userday);

    int deleteAll(String userid);

}
