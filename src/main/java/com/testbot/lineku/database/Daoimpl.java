package com.testbot.lineku.database;

import com.testbot.lineku.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

public class Daoimpl implements Dao {
    //query untuk table user

    private final String usertable = "tbl_user";
    private final String sqlselectall = "SELECT user_id, line_id, display_name FROM "
            + usertable;
    private final String sqlgetbyuserid = sqlselectall
            + " WHERE LOWER(user_id) LIKE LOWER(?);";
    private final String sqlregister = "INSERT INTO " + usertable
            + " (user_id, line_id, display_name) VALUES (?, ?, ?);";
    private final JdbcTemplate mjdbc;
    private final ResultSetExtractor<List<User>> multipleExtractor =
            new ResultSetExtractor<List<User>>() {
        @Override
        public List<User> extractData(ResultSet resultSet)
                throws SQLException, DataAccessException {
            List<User> list = new Vector<User>();
            while (resultSet.next()) {
                User p = new User(
                        resultSet.getString("user_id"),
                        resultSet.getString("line_id"),
                        resultSet.getString("display_name"));
                list.add(p);
            }
            return list;
        }
    };


    public Daoimpl(DataSource dataSource) {
        mjdbc = new JdbcTemplate(dataSource);
    }

    public List<User> get() {
        return mjdbc.query(sqlselectall, multipleExtractor);
    }


    public List<User> getByUserId(String userid) {
        return mjdbc.query(sqlgetbyuserid, new Object[]{userid}, multipleExtractor);
    }


    public int registerLineId(String userid, String lineid, String displayname) {
        return mjdbc.update(sqlregister, new Object[]{userid, lineid, displayname});
    }


}