package com.testbot.lineku.database;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.LineSignatureValidator;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;



@Configuration
@PropertySource("classpath:application.properties")
public class Config {

    @Autowired
    private Environment menv;

    /** Get Channel Secret.
     */
    @Bean(name = "com.linecorp.channel_secret")
    public String getChannelSecret() {
        return menv.getProperty("com.linecorp.channel_secret");
    }

    /** Get Channel Access Token.
     */
    @Bean(name = "com.linecorp.channel_access_token")
    public String getChannelAccessToken() {
        return menv.getProperty("com.linecorp.channel_access_token");
    }

    /** Get Messaging Client.
     */
    @Bean(name = "lineMessagingClient")
    public LineMessagingClient getMessagingClient() {
        return LineMessagingClient
                .builder(getChannelAccessToken())
                .apiEndPoint("https://api.line.me/")
                .connectTimeout(10_000)
                .readTimeout(10_000)
                .writeTimeout(10_000)
                .build();
    }

    /** Get Signature Validator.
     */
    @Bean(name = "lineSignatureValidator")
    public LineSignatureValidator getSignatureValidator() {
        return new LineSignatureValidator(getChannelSecret().getBytes());
    }

    /** Get Data Source.
     */
    @Bean
    DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        String url = "jdbc:postgresql://ec2-34-200-101-236.compute-1.amazonaws.com:5432/";
        String url1 = "dab0il55p65j0e?user=dnordlnmxkjerp&password=";
        String url2 = "5fe5e13f13406249c3a3d6abf5d94ff3";
        String url3 = "7d7f41b4daa0cde3a8a559be2b64a63c&sslmode=require";
        String urlHasil = url + url1 + url2 + url3;
        dataSource.setUrl(urlHasil);
        dataSource.setUsername("dnordlnmxkjerp");
        dataSource.setPassword("5fe5e13f13406249c3a3d6abf5d94ff37d7f41b4daa0cde3a8a559be2b64a63c");

        return dataSource;

    }

    /** Get DataBase User.
     */
    @Bean
    public Dao getPersonDao() {
        return new Daoimpl(getDataSource());
    }

    /** Get DataBase Workout.
     */
    @Bean
    public Workoutdb getSpendingDao() {
        return new Workoutdbimpl(getDataSource());
    }

}