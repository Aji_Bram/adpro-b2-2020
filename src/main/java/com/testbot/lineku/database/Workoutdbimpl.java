package com.testbot.lineku.database;

import com.testbot.lineku.model.Workoutplanner;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;


public class Workoutdbimpl implements Workoutdb {

    private final String workoutplantable = "workout_plan";
    private final String sqlselectall = "SELECT * FROM " + workoutplantable;


    private final String sqlregister = "INSERT INTO " + workoutplantable
            + " (user_id, userday, userworkout,"
            + " userset, userrep, totalworkout) "
            + "VALUES (?, ?, ?, ?, ?,?);";

    private final String updatetotal = "UPDATE " + workoutplantable + " SET "
            + "totalworkout = (?) WHERE (user_id) = (?) AND "
            + "(userday) = (?);";

    private final String deleteexercise = "DELETE FROM " + workoutplantable
            + " WHERE (user_id) = (?) AND (userday) = (?);";
    private final String deleteexerciseall = "DELETE FROM " + workoutplantable
            + " WHERE (user_id) = (?);";




    private final ResultSetExtractor<List<Workoutplanner>> multiplersextractor =
            new ResultSetExtractor<List<Workoutplanner>>() {
             
        @Override
        public List<Workoutplanner> extractData(ResultSet resultSet)
                throws SQLException, DataAccessException {
            List<Workoutplanner> list = new Vector<Workoutplanner>();
            while (resultSet.next()) {
                Workoutplanner sp = new Workoutplanner(
                    resultSet.getString("user_id"),
                    resultSet.getString("userday"),
                    resultSet.getString("userworkout"),
                    resultSet.getInt("userset"),
                    resultSet.getInt("userrep"),
                    resultSet.getInt("totalworkout"));
                list.add(sp); 
            } 
            return list; 
        }
    };

    private final JdbcTemplate jdbcTemplate;

    public Workoutdbimpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public List<Workoutplanner> getbyuserid(String userid) {
        return jdbcTemplate.query(sqlselectall
                + " WHERE LOWER(user_id) LIKE LOWER(?);",
                new Object[]{"%" + userid + "%"},
                multiplersextractor);
    }

    @Override
    public int saverecord(String userid, String userday,
                          String workoutname, int workoutset,
                          int workoutrep, int totalworkout) {
        return jdbcTemplate.update(sqlregister, userid, userday,
                workoutname, workoutset,
                workoutrep, totalworkout);
    }

    @Override
    public int updatetotal(String userid, String day, int total) {
        return jdbcTemplate.update(updatetotal, total, userid, day);
    }

    @Override
    public int delete(String userid, String userday) {
        return jdbcTemplate.update(deleteexercise, userid, userday);
    }

    @Override
    public int deleteAll(String userid) {
        return jdbcTemplate.update(deleteexerciseall, userid);
    }


}
