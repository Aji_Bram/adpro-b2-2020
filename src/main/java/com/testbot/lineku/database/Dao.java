package com.testbot.lineku.database;

import com.testbot.lineku.model.User;
import java.util.List;

public interface Dao {
    List<User> get();

    List<User> getByUserId(String userid);

    int registerLineId(String userid, String lineid, String displayname);



}
